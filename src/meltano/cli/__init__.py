from .main import cli
from . import extract, schema, discovery, initialize, add, install


def main():
    cli()
